# Virtual Space and Gallery

### Thinking of a space to show and house art pieces or installation in a virtual world. 

As of now, ideas are being thrown around.

Is it going to be a gallery space emulating the white cube esthetics ?

Is it going to explore alternate ways to visit an exhibition ?

Is it a platform to show art pieces ?

Is it a Art portfolio ?

**itch.io page:** https://meticulous-manta.itch.io/virtual-gallery-space

**Made with Godot**


